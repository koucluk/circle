package com.yascode.circle;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnDraw;
    EditText etRadius, etElemen;
    LinearLayout holder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnDraw = (Button) findViewById(R.id.btn_draw);
        etElemen = (EditText) findViewById(R.id.et_elemen);
        etRadius = (EditText) findViewById(R.id.et_radius);
        holder = (LinearLayout) findViewById(R.id.holder_view);

        btnDraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etElemen.getText().toString().equals("")
                        || etRadius.getText().toString().equals("")) {

                    Toast.makeText(MainActivity.this, "Please Complete required fields", Toast.LENGTH_SHORT).show();
                } else {
                    int rad = Integer.parseInt(etRadius.getText().toString());
                    int sum = Integer.parseInt(etElemen.getText().toString());
                    Circle crc = new Circle(getApplicationContext(), rad, sum);

                    if (holder.getChildCount() > 0) {
                        holder.removeAllViews();
                        holder.invalidate();

                    }
                    holder.addView(crc);
                }
            }
        });
    }
}
