package com.yascode.circle;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toRadians;

/**
 * Created by Koucluck on 10/5/2016.
 */

public class Circle extends View {

    Paint paint, stPaint, nextPaint;
    int radius, sum;
    ArrayList<Point> points;

    public Circle(Context context, int radius, int sum) {
        super(context);

        this.radius = radius;
        this.sum = sum;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        points = new ArrayList<>();

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.BLUE);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2f);

        stPaint = new Paint();
        stPaint.setAntiAlias(true);
        stPaint.setColor(Color.GREEN);
        stPaint.setStyle(Paint.Style.FILL);
        stPaint.setStrokeWidth(2f);

        nextPaint = new Paint();
        nextPaint.setAntiAlias(true);
        nextPaint.setColor(Color.BLUE);
        nextPaint.setStyle(Paint.Style.FILL);
        nextPaint.setStrokeWidth(2f);

        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, radius, paint);

        float x0 = this.getX() + this.getWidth() / 2;
        float y0 = this.getY() + this.getHeight() / 2;
        Log.d("X", "" + x0);

        for (int x = 0; x < sum; x++) {
            Point point = new Point();

            int angle = x * (360 / sum);
            point.x = (int) (x0 + radius * cos(toRadians(angle)));
            point.y = (int) (y0 + radius * sin(toRadians(angle)));

            if (x == 0) {
                canvas.drawCircle(point.x, point.y, 10, stPaint);
//                canvas.drawCircle(0, 0, 10, stPaint);
            } else {
                canvas.drawCircle(point.x, point.y, 10, nextPaint);
            }

            this.setRotation(-90);
        }
    }
}
